package com.company;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

/**
 * Created by ikustov on 20.04.17.
 */
public class CollectionUtilities {
    public static <T> List<T> list() {
        return Collections.emptyList();
    }

    public static <T> List<T> list(final T t) {
        return Collections.singletonList(t);
    }

    public static <T> List<T> list(final List<T> ts) {
        return Collections.unmodifiableList(new ArrayList<>(ts));
    }

    @SafeVarargs
    public static <T> List<T> list(final T... t) {
        return Collections.unmodifiableList(Arrays.asList(Arrays.copyOf(t, t.length)));
    }

    public static <T> T head(final List<T> list) {
        if (list.size() == 0) {
            throw new IllegalStateException("head of empty list");
        } else {
            return list.get(0);
        }
    }

    public static <T> List<T> copy(final List<T> ts) {
        return new ArrayList<>(ts);
    }

    public static <T> List<T> tail(final List<T> list) {
        final List<T> workList = copy(list);
        workList.remove(0);
        return Collections.unmodifiableList(workList);
    }

    public static Integer fold(final List<Integer> is, final Integer identity, final Function<Integer, Function<Integer, Integer>> f) {
        int result = identity;
        for (final Integer i : is) {
            result = f.apply(result).apply(i);
        }
        return result;
    }

    public static <T, U> U foldLeft(final List<T> ts, final U identity, final Function<U, Function<T, U>> f) {
        U result = identity;
        for (final T t : ts) {
            result = f.apply(result).apply(t);
        }
        return result;
    }

    public static <T> List<T> reverse(final List<T> list) {
        return foldLeft(list, list(), x -> y -> prepend(y, x));
    }

    public static <T, U> List<U> mapViaFoldLeft(final List<T> list, final Function<T, U> f) {
        return foldLeft(list, list(), x -> y -> append(x, f.apply(y)));
    }

    public static <T, U> List<U> mapViaFoldRight(final List<T> list, final Function<T, U> f) {
        return foldRight(list, list(), x -> y -> prepend(f.apply(x), y));
    }

    public static <T, U> U foldRight(final List<T> ts, final U identity, final Function<T, Function<U, U>> f) {
        return ts.isEmpty() ? identity : f.apply(head(ts)).apply(foldRight(tail(ts), identity, f));
    }

    public static <T> List<T> append(final List<T> list, final T t) {
        final List<T> ts = copy(list);
        ts.add(t);
        return Collections.unmodifiableList(ts);
    }

    public static <T> List<T> prepend(final T t, final List<T> list) {
        return foldLeft(list, list(t), a -> b -> append(a, b));
    }

    public static void main(final String[] args) {
        final Function<Double, Double> addTax = x -> x * 1.09;
        final Function<Double, Double> addShipping = x -> x + 3.50;
        final List<Double> prices = list(10.10, 23.45, 32.07, 9.23);
        final List<Double> pricesIncludingTax = mapViaFoldLeft(prices, addTax);
        final List<Double> pricesIncludingShipping = mapViaFoldLeft(pricesIncludingTax, addShipping);
        System.out.println(pricesIncludingShipping);
        System.out.println(mapViaFoldLeft(prices, addShipping.compose(addTax)));

        final Effect<Double> printWith2Decimals = x -> System.out.printf("%.2f", x);

        final Function<Executable, Function<Executable, Executable>> compose = x -> y -> () -> {
            x.exec();
            y.exec();
        };

        final Executable program = foldLeft(pricesIncludingShipping, () -> {
        }, e -> d -> compose.apply(e).apply(() -> printWith2Decimals.apply(d)));

        program.exec();

    }

    public static <T> void forEach(final Collection<T> ts, final Effect<T> e) {
        for (final T t : ts) e.apply(t);
    }

    public static List<Integer> range(final Integer start, final Integer end) {
        return end <= start ? CollectionUtilities.list() : CollectionUtilities.prepend(start, range(start + 1, end));
    }

    public static <T> List<T> unfold(final T seed, final Function<T, T> f, final Function<T, Boolean> p) {
        List<T> result = new ArrayList<>();
        T temp = seed;
        while (p.apply(temp)) {
            result = append(result, temp);
            temp = f.apply(temp);
        }
        return result;
    }


}
