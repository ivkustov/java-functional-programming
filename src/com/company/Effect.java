package com.company;

public interface Effect<T> {
    void apply(T t);
}
