package com.company;

public interface Executable {
    void exec();
}
