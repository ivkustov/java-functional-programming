package com.company;

public class Main {

    public static void main(final String[] args) {
        final double tax = 0.09;

        final Function<Double, Function<Double, Double>> addTax = taxRate -> price -> price + price * taxRate;
        System.out.println(addTax.apply(tax).apply(12.0));

    }

    public Function<Integer, Integer> factorial = n -> n <= 1 ? n : n * this.factorial.apply(n - 1);


    static <T, U, V> Function<Function<U, V>, Function<Function<T, U>, Function<T, V>>> higherCompose() {
        return (Function<U, V> f) -> (Function<T, U> g) -> (T x) -> f.apply(g.apply(x));
    }

    static <T, U, V> Function<Function<T, U>, Function<Function<U, V>, Function<T, V>>> higherAndThen() {
        return (Function<T, U> f) -> (Function<U, V> g) -> (T x) -> g.apply(f.apply(x));
    }

    public static void testHigherCompose() {
        final Function<Double, Integer> f = a -> {
            final double b = (a * 3);
            System.out.println(b);
            return (int) b;
        };
        final Function<Long, Double> g = a -> Double.valueOf(a + 2);
        System.out.println(f.apply(g.apply(1L)));
        System.out.println((Main.<Long, Double, Integer>higherCompose().apply(f).apply(g).apply(1L)));
    }

    <A, B, C, D> String func(final A a, final B b, final C c, final D d) {
        return String.format("%s,%s,%s,%s", a, b, c, d);
    }

    <A, B, C, D> Function<A, Function<B, Function<C, Function<D, String>>>> f() {
        return a -> b -> c -> d -> String.format("%s, %s, %s, %s", a, b, c, d);
    }

    public static void makeFunction() {
        final Function<Integer, Function<Boolean, Double>> function = x -> y -> {
            y = Boolean.valueOf(x > 0) && y;
            return Double.valueOf(y.toString());
        };
        final Function<Boolean, Function<Integer, Double>> function2 = reverseArgs(function);

        System.out.println(function.apply(1).apply(true));
    }

    public static <T, U, V> Function<U, Function<T, V>> reverseArgs(final Function<T, Function<U, V>> f) {
        return u -> t -> f.apply(t).apply(u);

    }

    static <T> Function<T, T> identity() {
        return t -> t;
    }

    Function<Integer, Double> addTwentyPrescent = x -> x * 1.2;

    static int add(int x, int y) {
        return y == 0 ? x : add(++x, --y);
    }


}
