package com.company;

public class OrderLine {
    public void setProduct(final Product product) {
        this.product = product;
    }

    public void setCount(final int count) {
        this.count = count;
    }

    private Product product;
    private int count;

    public OrderLine(final Product product, final int count) {
        super();
        this.product = product;
        this.count = count;
    }

    public Product getProduct() {
        return this.product;
    }

    public int getCount() {
        return this.count;
    }

    public Weight getWeight() {
        return this.product.getWeight().mult(this.count);
    }

    public Price getAmount() {
        return this.product.getPrice().mult(this.count);
    }


}
