package com.company;

public class Price {
    public static final Price zero = new Price(0.0);
    public final double value;

    public Price(final double value) {
        this.value = value;
    }

    Price add(final Price that) {
        return new Price(this.value + that.value);
    }

    Price mult(final int count) {
        return new Price(this.value * count);
    }


}
