package com.company;

/**
 * Created by ikustov on 20.04.17.
 */
public class Product {
    public String getName() {
        return name;
    }

    public Price getPrice() {
        return price;
    }

    public Weight getWeight() {
        return weight;
    }

    private final String name;
    private final Price price;
    private final Weight weight;

    public Product(final String name, final Price price, final Weight weight) {
        this.name = name;
        this.price = price;
        this.weight = weight;
    }
}
