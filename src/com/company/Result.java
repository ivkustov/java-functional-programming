package com.company;

public interface Result<T> {
    void bind(Effect<T> success, Effect<String> failure);

    public static <T> Result<T> failure(final String message) {
        return new Failure<>(message);
    }

    public static <T> Result<T> success(final T value) {
        return new Success<>(value);
    }

    public class Success<T> implements Result<T> {
        private final T value;

        private Success(final T t) {
            value = t;
        }

        @Override
        public void bind(final Effect<T> success, final Effect<String> failure) {
            success.apply(value);
        }
    }

    public class Failure<T> implements Result<T> {
        private final String errorMessage;

        public Failure(final String s) {
            this.errorMessage = s;
        }

        @Override
        public void bind(final Effect<T> success, final Effect<String> failure) {
            failure.apply(errorMessage);
        }
    }

}
