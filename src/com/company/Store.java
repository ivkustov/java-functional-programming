package com.company;

import java.util.List;

import static com.company.CollectionUtilities.foldLeft;
import static com.company.CollectionUtilities.list;

/**
 * Created by ikustov on 20.04.17.
 */
public class Store {

    public static void main(final String[] args) {
        final Product toothBrush = new Product("Tooth brust", new Price(3.5), new Weight(0.3));
        final Product toothPaste = new Product("Tooth paste", new Price(1.5), new Weight(0.5));
        final List<OrderLine> order = list(new OrderLine(toothPaste, 2), new OrderLine(toothBrush, 3));
        final Weight weight = foldLeft(order, Weight.ZERO, x -> y -> x.add(y.getWeight()));
        final Price price = foldLeft(order, Price.zero, x -> y -> x.add(y.getAmount()));
        System.out.println(price);
        System.out.println(weight);
    }
}
