package com.company;

import java.util.function.Supplier;

/**
 * Created by ikustov on 20.04.17.
 */
public abstract class TailCall<T> {
    private static class Return<T> extends TailCall<T> {
        private final T t;

        public Return(final T t) {
            this.t = t;
        }

        @Override
        public TailCall<T> resume() {
            throw new IllegalStateException();
        }

        @Override
        public T eval() {
            return t;
        }

        @Override
        public boolean isSuspend() {
            return false;
        }
    }

    public static class Suspend<T> extends TailCall<T> {
        private final Supplier<TailCall<T>> resume;

        private Suspend(final Supplier<TailCall<T>> resume) {
            this.resume = resume;
        }

        @Override
        public TailCall<T> resume() {
            return resume.get();
        }

        @Override
        public T eval() {
            TailCall<T> tailRec = this;
            while (tailRec.isSuspend()) {
                tailRec = tailRec.resume();
            }
            return tailRec.eval();
        }

        @Override
        public boolean isSuspend() {
            return true;
        }
    }


    public abstract TailCall<T> resume();

    public abstract T eval();

    public abstract boolean isSuspend();

    static TailCall<Integer> add(final int x, final int y) {
        return y == 0 ? new TailCall.Return<>(x) : new TailCall.Suspend<>(() -> add(x + 1, y - 1));
    }

    public static void main(final String[] args) {
        final TailCall<Integer> tailCall = add(3, 2);
        while (tailCall.isSuspend()) {
            tailCall.resume();
        }
        System.out.println(tailCall.eval());
    }
}
