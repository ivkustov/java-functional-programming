package com.company;

/**
 * Created by ikustov on 19.04.17.
 */
public class Tuple<T, U> {
    public final T _1;
    public final U _2;

    public Tuple(final T t, final U u) {
        this._1 = t;
        this._2 = u;

    }
}
