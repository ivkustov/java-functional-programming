package com.company;

/**
 * Created by ikustov on 20.04.17.
 */
public class Weight {
    public static final Weight ZERO = new Weight(0.0);
    public final double value;

    public Weight(final double value) {
        this.value = value;
    }

    public Weight mult(final int count) {
        return new Weight(value * count);
    }

    Weight add(final Weight that) {
        return new Weight(this.value + that.value);
    }
}
